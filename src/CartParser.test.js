import CartParser from './CartParser';
import path from 'path';

const parentDir = path.resolve();
const resourceDir = path.join(parentDir, 'samples');
const resourceFile = path.join(resourceDir, 'cart.csv');

const notValidResourceDir = path.join(resourceDir, 'not-valid');
const notValidHeaderFile = path.join(notValidResourceDir, 'header.csv');
const notValidRowFile = path.join(notValidResourceDir, 'row.csv');
const notValidStringCellFile = path.join(notValidResourceDir, 'cell-not-string.csv');
const notValidNumberPositiveCellFile = path.join(notValidResourceDir, 'cell-not-positive-number.csv');

let parser, ErrorType;

beforeEach(() => {
    parser = new CartParser();
    ErrorType = parser.ErrorType;
});

describe('CartParser - unit tests', () => {
    describe('validate', () => {
        it('should return array contains error, type of `header`, when a header item is not valid', () => {
            const contents = parser.readFile(notValidHeaderFile);
            const [err] = parser.validate(contents);

            expect(err).toHaveProperty('type', ErrorType.HEADER);
        });

        it('should return array contains error, type of `row`, when the amount of rows is not valid', () => {
            const contents = parser.readFile(notValidRowFile);
            const [err] = parser.validate(contents);

            expect(err).toHaveProperty('type', ErrorType.ROW);
        });

        describe('when a cell item doesn\'t match to a valid type', () => {
            it('should return array contains error with `Expected cell to be a nonempty string...` message', () => {
                const contents = parser.readFile(notValidStringCellFile);
                const [err] = parser.validate(contents);

                expect(err.message).toMatch('Expected cell to be a nonempty string');
                expect(err).toHaveProperty('type', ErrorType.CELL);
            });

            it('should return array contains error with `Expected cell to be a positive number...` message', () => {
                const contents = parser.readFile(notValidNumberPositiveCellFile);
                const [err] = parser.validate(contents);

                expect(err.message).toMatch('Expected cell to be a positive number');
                expect(err).toHaveProperty('type', ErrorType.CELL);
            });
        });

        it('should return an empty array, when a file contains a valid schema', () => {
            const contents = parser.readFile(resourceFile);
            const errors = parser.validate(contents);

            expect(errors).toEqual([]);
        });
    });

    describe('parseLine', () => {
        it('should properly parse a valid csv line', () => {
            const csvLine = 'Apple, 3.14, 41';
            const {id, ...parsedObj} = parser.parseLine(csvLine);

            expect(parsedObj).toEqual({name: 'Apple', price: 3.14, quantity: 41});
            expect(id).toBeTruthy(); // also might be replaced with uuid regexp
        });
    });

    describe('calcTotal', () => {
        it('should properly calculate the price total', () => {
            const items = [
                {
                    price: 4.5,
                    quantity: 2,
                },
                {
                    price: 0.25,
                    quantity: 4,
                },
                {
                    price: 4.2,
                    quantity: 1,
                }];
            const total = parser.calcTotal(items);

            expect(total).toBe(14.2);
        });
    })
});

describe('CartParser - integration test', () => {
    it('should throw an error, when the file contains the wrong schema', () => {
        expect(() => parser.parse(notValidHeaderFile)).toThrow('Validation failed!');
    });

    it('should return a valid the total price', () => {
        const {total} = parser.parse(resourceFile);

        expect(total).toBe(348.32);
    });

    it('should return a valid cart items', () => {
        const {items} = parser.parse(resourceFile);

        expect(items[0]).toMatchObject({
            name: 'Mollis consequat',
            price: 9,
            quantity: 2,
        });

        expect(items[1]).toMatchObject({
            name: 'Tvoluptatem',
            price: 10.32,
            quantity: 1,
        });

        expect(items[2]).toMatchObject({
            name: 'Scelerisque lacinia',
            price: 18.9,
            quantity: 1,
        });

        expect(items[3]).toMatchObject({
            name: 'Consectetur adipiscing',
            price: 28.72,
            quantity: 10,
        });

        expect(items[4]).toMatchObject({
            name: 'Condimentum aliquet',
            price: 13.9,
            quantity: 1,
        });
    });
})








